module ApplicationHelper
    
    # ページごとに完全なタイトルの設定
    def full_title(page_title = '')
        base_title = "Catter App"
        if page_title.empty?
            base_title
        else
            page_title + " | " + base_title
        end
    end
end
