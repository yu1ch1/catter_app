Rails.application.routes.draw do
  root 'home_pages#home'
  get     '/help',    to: 'home_pages#help'
  get     '/about',   to: 'home_pages#about'
  get     '/contact', to: 'home_pages#contact'
  get     '/signup',  to: 'users#new'
  post    'signup',   to: 'users#create'
  get     '/login',   to: 'sessions#new' 
  post    '/login',   to: 'sessions#create'
  delete  '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :relationships,       only: [:create, :destroy]
  
  resources :microposts do
    resources :likes, only: [:create, :destroy]
  end
end
